import { Component, OnInit } from '@angular/core';
import IPFS from 'ipfs-http-client';
import { Buffer } from 'buffer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  public title: string = 'Angular 7';

  uploadAddress: string = 'https://ipfs.infura.io/ipfs/';

  filename: string = null;
  prevFilename: string = null;
  filesize: number = null;
  filetype: any;
  imagePreview: any;
  fname: any[];
  fileUpload: any;
  linkAddressGenerated: string = null;
  show: boolean = null;

  selectedFile = null;
  file: any;

  uploadResult: any;
  text: any;

  constructor() { 
  }

  ngOnInit() {
    this.show = true;
  }

  openOptions() {
  	window.document.getElementById('dashboard-button').click();
  }

  onFileSelected(event){
    event.preventDefault();
    
    this.file = event.target.files[0];
    this.filetype = this.file.type.slice(0,5).toString();

    this.filename = event.target.files[0].name;
    this.prevFilename = event.target.files[0].name;
    this.filesize = event.target.files[0].size;

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsArrayBuffer(file) 
    reader.onloadend = () => {
      this.fileUpload = Buffer.from(<string>reader.result);
    }

  }

  removeFile(){
    this.filename = null;
    console.log('Removed');
  }

  onUpload(){
    event.preventDefault();
    this.show = false;
    console.log('Pressed!');
    const ipfs = new IPFS({host: 'ipfs.infura.io', port: 5001, protocol: 'https'});
    const data = this.fileUpload;

    ipfs.add(data, (error, result) => {
      
      if(error){
        return console.log(error);
      }
      console.log(this.uploadAddress + result[0].hash);

      if(this.filetype === 'image') {
        this.linkAddressGenerated = this.uploadAddress + result[0].hash;
        this.imagePreview = this.uploadAddress + result[0].hash;
      } else {
        this.linkAddressGenerated = this.uploadAddress + result[0].hash;
        this.imagePreview = '../../../assets/arrow-download-icon.png';
      }
      
      this.filename = null;
      this.show = true;
    })
  }

  /* To copy Text from Textbox */
  copyInputMessage(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

}
